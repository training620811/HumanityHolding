public class NonProfitCompany extends Company implements ITaxPayment {


    public NonProfitCompany(int companyNo, String name, String city, String service, String aim) {
        super(companyNo, name, city, service, aim);
    }

    @Override
    public int giveTaxt() {
        return super.tax = 5 ;
    }

    @Override
    public String toString() {
        return  "Name: " + this.name +
                "\nCompany No: " + this.companyNo +
                "\nCity: " + super.city +
                "\nService: " + this.service +
                "\nAim: " + this.aim +
                "\nTax: " + this.giveTaxt();
    }
}
