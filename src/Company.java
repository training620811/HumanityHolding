public class Company {
    protected int companyNo;
    protected String name;
    protected String city;
    protected String service;
    protected String aim;
    protected int tax;

    public Company(int companyNo, String name, String city, String service, String aim) {
        this.companyNo = companyNo;
        this.name = name;
        this.city = city;
        this.service = service;
        this.aim = aim;
        this.tax = 0;



    }

}
